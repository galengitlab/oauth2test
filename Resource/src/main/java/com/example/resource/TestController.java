package com.example.resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @GetMapping("/noauth")
    public String noauth(){
        return "noauth";
    }

    @GetMapping("/test")
    public String test(){
        return "test";
    }

    @GetMapping("/aaa")
    public String aaa(){
        return "aaa";
    }
}
