package com.example.authorization.security;

import com.example.authorization.model.User;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class MyAuthenticationProvider implements AuthenticationProvider {

    private final PasswordEncoder passwordEncoder;

    public MyAuthenticationProvider(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        var username = authentication.getName();
        var password = (String) authentication.getCredentials();
        System.out.println(username + password);
        System.out.println("---------------------------------");
        if (!username.equals("user") || !password.equals("123")) {
            throw new BadCredentialsException("GGGGGGGGGGGGGGGG");
        }

        var user = new User().setUserId(1L).setUsername("user").setPassword("123");
        var permissions = new String[]{"permission1", "permission2"};
        var xxx = new UsernamePasswordAuthenticationToken(user, password, AuthorityUtils.createAuthorityList(permissions));
        System.out.println(xxx);
        return xxx;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
    }
}
